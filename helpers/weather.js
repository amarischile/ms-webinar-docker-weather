import utils from './utils'
const moment = require('moment')

const transform = (data) => {
    let weather = ''
    let icon = null
    if(data.weather.length > 0){
        weather = data.weather[0].description
        weather = weather.charAt(0).toUpperCase() + weather.slice(1)
        icon =  data.weather[0].icon
    }
    const date = new Date( new Date().getTime() + ( new Date().getTimezoneOffset() * data.timezone) + (3600000));

    return {
        weather: {
            description: weather,
            icon: `http://openweathermap.org/img/wn/${icon}@2x.png`,
        },
        temperature: {
            kelvin: Math.round(data.main.temp),
            celcius: Math.round(utils.kelvinToCelcius(data.main.temp)),
            fahrenheit: Math.round(utils.celciusToFahrenheit(utils.kelvinToCelcius(data.main.temp))),
        },
        city: data.name,
        country: {
            code: data.sys.country,
        },
        wind: {
            speed: data.wind.speed,
        },
        humidity: data.main.humidity, 
        date:{
            formated: moment(date).format('dddd, LT'),
        }  
    }
}

export default {
    transform
} 