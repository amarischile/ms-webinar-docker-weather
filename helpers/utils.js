
const kelvinToCelcius = (grades) =>{
    return grades - 273.15
} 

const celciusToFahrenheit = (grades) => {
    return (9*grades)/5 + 32
} 

export default {
    kelvinToCelcius,
    celciusToFahrenheit,
}