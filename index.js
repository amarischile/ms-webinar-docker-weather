import api from '@amacl/roboc-api'
import weather from './services/weather'
import weatherHelper from './helpers/weather'
var cors = require('cors')

api({
    prefix: process.env.API_PREFIX || '/',        
    port: process.env.PORT || 8000,
    host: process.env.HOST || '0.0.0.0',
    alive: '/health',
},(router) => {

     router.get('/',cors(), async (req, res, next) => {
        const wrapper = weatherHelper.transform(await weather.getByCity(process.env.OPENWEATHERMAP_CITY,process.env.OPENWEATHERMAP_COUNTRY_ISO))         
        //const wrapper = weather.getByCityMock('Santiago','cl')         
        res.status(200).json(wrapper)        
    })
 
    return router
})