const fetch = require('node-fetch')
const getByCity = async (city, countryIso) =>{
    let data = null
    try{
        const api = process.env.OPENWEATHERMAP_API
            .replace('{apikey}',process.env.OPENWEATHERMAP_APIKEY)
            .replace('{city',`${city},${countryIso.toLowerCase()}`)
        const response = await fetch(api,{
            method: 'GET'
        })
        data = await response.json()
    }catch(e){
        console.log('getByCity', e)
    }
    return data
}

const getByCityMock = () => {
    return {
        "weather": [
            {
                "title": "Clear",
                "description": "clear sky"
            }
        ],
        "temperature": {
            "kelvin": 281.15,
            "celcius": 8,
            "fahrenheit": 46.4
        },
        "city": "Santiago",
        "country": {
            "code": "CL"
        },
        "wind": {
            "speed": 1.5
        },
        "humidity": 87
    }
}

export default {
    getByCity,
    getByCityMock,
}