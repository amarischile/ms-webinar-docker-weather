# MS WEATHER

Micro servicio Wather, encargado de obtener el clima por pais.
Para su construccion se utiliza un `Dockerfile` en donde la imagen base es `node:12-slim`.

## Container Configuration

### BUILD

Para la contruccion del contenedor es necesario ejecutar el siguiente comando:

```sh
docker build -t webinar-docker/ms-weather .
```

Flag `-t` es utilizado para crear un tag de la imagen


### ENV
Variables de entorno necesarias para la correcta ejecucion del contenedor

```sh
TZ=America/Santiago
PORT=8000
OPENWEATHERMAP_APIKEY=76c2ded8eebab009621ef9610f7fae16
OPENWEATHERMAP_API=https://api.openweathermap.org/data/2.5/weather?q={city}&appid={apikey}
OPENWEATHERMAP_CITY=Santiago
OPENWEATHERMAP_COUNTRY_ISO=cl
```

* TZ: Define zona horaria (America/Santiago)
* PORT: Puerto en que escucha el ms
* OPENWEATHERMAP_APIKEY: API KEY de OpenWeatherMap
* OPENWEATHERMAP_API: URL del API utilizada
* OPENWEATHERMAP_CITY: Ciudad de la cual se quiere obtener el clima
* OPENWEATHERMAP_COUNTRY_ISO: Pais del cual se quiere obtener el clima

### PORT FORWARDING

Contenedor se instanciará en el puerto `8000`

```sh
docker run -p 8000:8000 ... ... ...
```

Se le indica que todo el trafico in/out por el puerto `8000` del host hacia el `8000` del contenedor.

## Container Execution

Para la ejecucion solo hace falta tener instalado Docker dentro de la maquina y en una consola ejecutar lo siguiente:

```sh
docker build -t webinar-docker/ms-weather .
```

Con esto crearemos la imagen llamada `webinar-docker/ms-weather` y con el `.` le decimos que contruya en base al Dockerfile del directorio.

Luego ejecutamos lo siguiente para instanciar el contenedor.

```sh
docker run --rm -p 8000:8000 \
    --name ms-webinar-docker-weather \
    --network webinar-docker \
    -e TZ=America/Santiago \
    -e PORT=8000 \
    -e OPENWEATHERMAP_APIKEY=76c2ded8eebab009621ef9610f7fae16 \
    -e OPENWEATHERMAP_API="https://api.openweathermap.org/data/2.5/weather?q={city}&appid={apikey}" \
    -e OPENWEATHERMAP_CITY=Santiago \
    -e OPENWEATHERMAP_COUNTRY_ISO=cl \
    webinar-docker/ms-weather
```

Esto ejecutará el contenedor de Docker en el puerto `8000` y le añade multiples variables de entorno y configuraciones.
En caso de querer ejecutar la imagen en background agregar luego del `-p 8000:8000` un `-d` o `--detached`.